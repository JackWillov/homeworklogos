package Lesson13.Compares;

import Lesson13.Commodity;

import java.util.Comparator;

public class CompareByWidth implements Comparator<Commodity> {

    @Override
    public int compare(Commodity o1, Commodity o2) {
        return o1.getWidth() - o2.getWidth();
    }
}
