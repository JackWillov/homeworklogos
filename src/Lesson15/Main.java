package Lesson15;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Map < Person , List<Animal>> personAnimalMap = new LinkedHashMap<>();

        Person person1 = new Person("Vasyl", 21);

        Person person2 = new Person("Yulian", 18);

        Animal animal1 = new Animal("cat", "bonny");

        Animal animal2 = new Animal("cat", "cezar");

        Animal animal3 = new Animal("dog", "taxa");

        Animal animal4 = new Animal("dog", "barbie");


        personAnimalMap.put(person1, new ArrayList<Animal>()) ;
            ArrayList <Animal> animalsList1 = new ArrayList<>();
            animalsList1.add(animal1);
            animalsList1.add(animal2);

            personAnimalMap.put(person1,animalsList1);

        personAnimalMap.put(person2, new ArrayList<Animal>()) ;
        ArrayList <Animal> animalsList2 = new ArrayList<>();
        animalsList2.add(animal3);
        animalsList2.add(animal4);

        personAnimalMap.put(person2,animalsList2);

for (Map.Entry<Person, List<Animal>> e : personAnimalMap.entrySet()){
    System.out.println( "Person " + e.getKey() + "Animal " + e.getValue());
}



            personAnimalMap.remove(person2);
        System.out.println();
        System.out.println("after i delete 1 person");
        System.out.println();
        for (Map.Entry<Person, List<Animal>> e : personAnimalMap.entrySet()) {
            System.out.println("Person " + e.getKey() + "Animal " + e.getValue());
        }
        System.out.println();
        System.out.println("now i delete 1 animal");
        System.out.println();
        personAnimalMap.get(person1).remove(1);
        for (Map.Entry<Person, List<Animal>> e : personAnimalMap.entrySet()) {
            System.out.println("Person " + e.getKey() + "Animal " + e.getValue());
        }
    }
}
