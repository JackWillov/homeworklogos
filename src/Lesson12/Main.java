package Lesson12;


import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        LinkedList<String> list = new LinkedList<>();
        int n = 0;

        while (true) {
            System.out.println("print word number : " + (n + 1));
            n++;
            String word = scan.next();
            if (word.equals("break")) break;
            list.add(word);
        }

        System.out.println("here is all words which u write");

        System.out.println(list);

        System.out.println();
        System.out.println("Words which fist letter is s or S");


        for (String s : list) {
            if (s.startsWith("s") || s.startsWith("S"))
                System.out.println(s);
        }


        System.out.println();
        System.out.println("here's all word which contain more than 5 letter");

        for (String s : list) {
            if (s.length() > 5)
                System.out.println(s);


        }
    }
}