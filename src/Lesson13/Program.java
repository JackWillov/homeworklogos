package Lesson13;

import Lesson13.Compares.ComparatorByLength;
import Lesson13.Compares.CompareByWeight;
import Lesson13.Compares.CompareByWidth;

import java.util.Comparator;
import java.util.LinkedList;

public class Program {

    LinkedList<Commodity> cm = new LinkedList<>();

    public void listAddProduct(String name1, int width1, int weight1, int length1){
        cm.add(new Commodity(name1, width1, weight1, length1));
    }
    public void deleteProduct(int productIndex){
        cm.remove(productIndex);
    }
    public void productChange(int oldProduct , String name1, int width1, int weight1, int length1){
        cm.remove(oldProduct);
        cm.add(cm.indexOf(oldProduct),new Commodity(name1, width1, weight1, length1));
    }

public void sortByName(){

    cm.sort(Comparator.naturalOrder());
}
    public void sortByWeight(){
        cm.sort(new CompareByWeight());
    }
    public void sortByLenght(){
        cm.sort(new ComparatorByLength());
    }
    public void sortByWidth(){
        cm.sort(new CompareByWidth());
    }
    public void printNumberedProduct(int choosenIndex){
        System.out.println(cm.get(choosenIndex));
    }
    public void printList(){
        cm.toString();
    }

}
