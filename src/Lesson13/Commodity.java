package Lesson13;

import java.util.LinkedList;


public class Commodity implements Comparable<Commodity> {


    String name;
    int length;
    int weight;
    int width;
    public int getWidth() {
        return width;
    }

    public int getWeight() {
        return weight;
    }

    public int getLength() {
        return length;
    }

    public String getName() {
        return name;
    }



    public Commodity(String name , int width , int weight , int length) {
        this.name = name;
        this.weight = weight;
        this.width = width;
        this.length = length;

    }

    @Override
    public String toString() {
        return "Commodity{ " +
                " name=' " + name + '\'' +
                ", length= " + length +
                ", weight= " + weight +
                ", width= " + width +
                '}';
    }


    @Override
    public int compareTo(Commodity o) {
        int res = this.name.compareTo(o.name);

        return res;
    }
}

